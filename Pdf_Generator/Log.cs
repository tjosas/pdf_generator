﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdf_Generator
{
    public class Log
    {
        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        public void LogSimpleEvent(string _applicationName, string _logType, string _message, string _textDump)
        {
            LogReference.PrecisePredictionLogServiceClient client = new LogReference.PrecisePredictionLogServiceClient();
            try
            {
                client.logEventSimple(_applicationName, _logType, _message, _textDump);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void LogEvent(string _applicationName, string _logType, string _message, string _textDump, byte[] _dataDump)
        {
            LogReference.PrecisePredictionLogServiceClient client = new LogReference.PrecisePredictionLogServiceClient();
            try
            {
                client.logEvent(_applicationName, _logType, _message, _textDump, _dataDump);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
