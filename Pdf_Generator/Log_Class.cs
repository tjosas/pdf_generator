﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Pdf_Generator
{
    class Log_Class
    {
        public void LogError(string message, string location, string textdump)
        {
            Dictionary<string, string> jsonDictionary = new Dictionary<string, string>();
            jsonDictionary.Add("type", "Error");
            jsonDictionary.Add("message", message);
            jsonDictionary.Add("location", location);
            jsonDictionary.Add("other", textdump);
            sendLogAsync(jsonDictionary, ",Error,pdfGenerator");
        }

        public void logJob(string message, string textdump)
        {
            Dictionary<string, string> jsonDictionary = new Dictionary<string, string>();
            jsonDictionary.Add("type", "Log");
            jsonDictionary.Add("message", message);
            jsonDictionary.Add("other", textdump);
            sendLogAsync(jsonDictionary, ",Log,pdfGenerator");
        }

        async void sendLogAsync(Dictionary<string, string> message, string tags)
        {
            Console.WriteLine("Log uploaded");
            using (var client = new HttpClient())
            {
                var content = new FormUrlEncodedContent(message);

                var response = await client.PostAsync(@"http://logs-01.loggly.com/inputs/b8a65059-8298-4cf5-9ac2-02330a32e8be/tag/net" + tags, content);

                var responseString = await response.Content.ReadAsStringAsync();
            }
        }
    }
}
