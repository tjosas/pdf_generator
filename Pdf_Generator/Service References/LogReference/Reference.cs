﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pdf_Generator.LogReference {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="LogReference.IPrecisePredictionLogService")]
    public interface IPrecisePredictionLogService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecisePredictionLogService/logEventSimple", ReplyAction="http://tempuri.org/IPrecisePredictionLogService/logEventSimpleResponse")]
        string logEventSimple(string _ApplicationName, string _LogType, string _Message, string _TextDump);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecisePredictionLogService/logEventSimple", ReplyAction="http://tempuri.org/IPrecisePredictionLogService/logEventSimpleResponse")]
        System.Threading.Tasks.Task<string> logEventSimpleAsync(string _ApplicationName, string _LogType, string _Message, string _TextDump);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecisePredictionLogService/logEvent", ReplyAction="http://tempuri.org/IPrecisePredictionLogService/logEventResponse")]
        string logEvent(string _ApplicationName, string _LogType, string _Message, string _TextDump, byte[] _DataDump);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecisePredictionLogService/logEvent", ReplyAction="http://tempuri.org/IPrecisePredictionLogService/logEventResponse")]
        System.Threading.Tasks.Task<string> logEventAsync(string _ApplicationName, string _LogType, string _Message, string _TextDump, byte[] _DataDump);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IPrecisePredictionLogServiceChannel : Pdf_Generator.LogReference.IPrecisePredictionLogService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class PrecisePredictionLogServiceClient : System.ServiceModel.ClientBase<Pdf_Generator.LogReference.IPrecisePredictionLogService>, Pdf_Generator.LogReference.IPrecisePredictionLogService {
        
        public PrecisePredictionLogServiceClient() {
        }
        
        public PrecisePredictionLogServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public PrecisePredictionLogServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PrecisePredictionLogServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PrecisePredictionLogServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string logEventSimple(string _ApplicationName, string _LogType, string _Message, string _TextDump) {
            return base.Channel.logEventSimple(_ApplicationName, _LogType, _Message, _TextDump);
        }
        
        public System.Threading.Tasks.Task<string> logEventSimpleAsync(string _ApplicationName, string _LogType, string _Message, string _TextDump) {
            return base.Channel.logEventSimpleAsync(_ApplicationName, _LogType, _Message, _TextDump);
        }
        
        public string logEvent(string _ApplicationName, string _LogType, string _Message, string _TextDump, byte[] _DataDump) {
            return base.Channel.logEvent(_ApplicationName, _LogType, _Message, _TextDump, _DataDump);
        }
        
        public System.Threading.Tasks.Task<string> logEventAsync(string _ApplicationName, string _LogType, string _Message, string _TextDump, byte[] _DataDump) {
            return base.Channel.logEventAsync(_ApplicationName, _LogType, _Message, _TextDump, _DataDump);
        }
    }
}
