﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdf_Generator
{
    class SalesforceObjects
    {
        public EnterpriseWsdl.Loan__c LoanObject
        {
            get;
            set;
        }
        public EnterpriseWsdl.Applicant__c ApplicantObject
        {
            get;
            set;
        }
        public EnterpriseWsdl.RePayment_Plan__c RepaymentObject
        {
            get;
            set;
        }

        public List<EnterpriseWsdl.Payment_Plan__c> BreakdownObjectList;

        public List<EnterpriseWsdl.Payment__c> PaymentObjectsList;

        public string LoanId
        {
            get;
            set;
        }

        public SalesforceObjects(string _loanId)
        {
            LoanId = _loanId;
            BreakdownObjectList = new List<EnterpriseWsdl.Payment_Plan__c>();
            PaymentObjectsList = new List<EnterpriseWsdl.Payment__c>();
        }

    }
}
