﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdf_Generator
{
    class Program
    {
        static void Main(string[] args)
        {
            SalesforceCommunicator sfComm = new SalesforceCommunicator();
            PdfGenerator pdfGen = new PdfGenerator();

            
            sfComm.downloadTemplates();
            Console.WriteLine("templates lastet");
            
            var files = sfComm.getLoansForPdfGenerating();
            Console.WriteLine("Hentet lån");

            if (files != null && files.Count > 0)
            {
                var letters = pdfGen.GeneratePdf(files);
                Console.WriteLine("generert pdf");

                sfComm.uploadPdfs(letters);
                Console.WriteLine("lastet opp pdf");

                pdfGen.deleteFiles();
            }
            Console.WriteLine("ferdig");
            //Console.ReadKey();
        }
    }
}
