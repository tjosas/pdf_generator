﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Word;
using System.IO;
using Word = Microsoft.Office.Interop.Word;
using System.Globalization;

namespace Pdf_Generator
{
    class PdfGenerator
    {
        public string FolderLocation { get; set; }

        private static string setPath
        {
            get
            {
                if (Directory.Exists(@"D:\Data\Cashflows Payments\"))
                {
                    return @"D:\Data\Cashflows Payments\";
                }
                else
                {
                    return @"C:\files\pdf\";
                }
            }
        }

        public Dictionary<string, byte[]> GeneratePdf(List<Tuple<EnterpriseWsdl.Loan__c, EnterpriseWsdl.FileAttachment__c, EnterpriseWsdl.Applicant__c, EnterpriseWsdl.RePayment_Plan__c, EnterpriseWsdl.Payment_Plan__c>> _files)
        {
            String filePath = setPath;
            Dictionary<string, byte[]> letters = new Dictionary<string, byte[]>();
            foreach (Tuple<EnterpriseWsdl.Loan__c, EnterpriseWsdl.FileAttachment__c, EnterpriseWsdl.Applicant__c, EnterpriseWsdl.RePayment_Plan__c, EnterpriseWsdl.Payment_Plan__c> item in _files)
            {
                Microsoft.Office.Interop.Word.Application appWord = new Microsoft.Office.Interop.Word.Application();
                string x = setPath + "templates\\" + item.Item2.FileTemplate__c;
                Microsoft.Office.Interop.Word.Document wordDocument = appWord.Documents.Open(setPath + "templates\\" + item.Item2.FileTemplate__c);

                textReplace(item, appWord, wordDocument.Content.Text);

                switch (item.Item2.FileType__c)
                {
                    case "Annual Statement":
                        insertTable(wordDocument, item.Item1.Id);
                        break;

                    default:
                        break;
                }

                wordDocument.ExportAsFixedFormat(setPath + "letters\\" + item.Item2.FileName__c, WdExportFormat.wdExportFormatPDF);

                byte[] pdfFile = File.ReadAllBytes(setPath + "letters\\" + item.Item2.FileName__c + ".pdf");
                letters.Add(item.Item2.Id, pdfFile);

                wordDocument.Close(false);
                int res = System.Runtime.InteropServices.Marshal.ReleaseComObject(wordDocument);
                int res1 = System.Runtime.InteropServices.Marshal.ReleaseComObject(appWord);
            }
            return letters;
        }

        private void textReplace(Tuple<EnterpriseWsdl.Loan__c, EnterpriseWsdl.FileAttachment__c, EnterpriseWsdl.Applicant__c, EnterpriseWsdl.RePayment_Plan__c, EnterpriseWsdl.Payment_Plan__c> _objects, Microsoft.Office.Interop.Word.Application appWord, string _text)
        {
            //Change date
            //findAndReplace(appWord, "{Today}", DateTime.Today.ToShortDateString());
            getDate("{Today}", _text, DateTime.Today, appWord);

            //Replace from loan
            if (!String.IsNullOrEmpty(_objects.Item1.Agent__c))
            {
                findAndReplace(appWord, "{loan__c.Agent__c}", _objects.Item1.Agent__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Agent__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.External_Collection_Company__c))
            {
                findAndReplace(appWord, "{loan__c.External_Collection_Company__c}", _objects.Item1.External_Collection_Company__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.External_Collection_Company__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.External_Collection_Status__c))
            {
                findAndReplace(appWord, "{loan__c.External_Collection_Status__c}", _objects.Item1.External_Collection_Status__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.External_Collection_Status__c}", "");
            }

            if (_objects.Item1.Amount_Paid__c != null)
            {
                findAndReplace(appWord, "{loan__c.Amount_Paid__c}", ((double)_objects.Item1.Amount_Paid__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Amount_Paid__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Applicant__c))
            {
                findAndReplace(appWord, "{loan__c.Applicant__c}", _objects.Item1.Applicant__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Applicant__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.ApplicationFormCheck__c))
            {
                findAndReplace(appWord, "{loan__c.AppliedOn__c}", _objects.Item1.AppliedOn__c.ToString());
            }
            else
            {
                findAndReplace(appWord, "{loan__c.AppliedOn__c}", "");
            }

            if (_objects.Item1.AppliedOn__c != null)
            {
                getDate("{loan__c.AppliedOn__c}", _text, (DateTime)_objects.Item1.AppliedOn__c, appWord);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.AppliedOn__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Bank_Account__c))
            {
                findAndReplace(appWord, "{loan__c.Bank_Account__c}", _objects.Item1.Bank_Account__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Bank_Account__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Callcredit_Status__c))
            {
                findAndReplace(appWord, "{loan__c.Callcredit_Status__c}", _objects.Item1.Callcredit_Status__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Callcredit_Status__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.CallReportCheck__c))
            {
                findAndReplace(appWord, "{loan__c.CallReportCheck__c}", _objects.Item1.CallReportCheck__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.CallReportCheck__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.CallValidateBankAndIDCheck__c))
            {
                findAndReplace(appWord, "{loan__c.CallValidateBankAndIDCheck__c}", _objects.Item1.CallValidateBankAndIDCheck__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.CallValidateBankAndIDCheck__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.CallValidateCardCheck__c))
            {
                findAndReplace(appWord, "{loan__c.CallValidateCardCheck__c}", _objects.Item1.CallValidateCardCheck__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.CallValidateCardCheck__c}", "");
            }

            if (_objects.Item1.Close_Date__c != null)
            {
                getDate("{loan__c.Close_Date__c}", _text, (DateTime)_objects.Item1.Close_Date__c, appWord);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Close_Date__c}", "");
            }

            if (_objects.Item1.Days_over_due__c != null)
            {
                findAndReplace(appWord, "{loan__c.Days_over_due__c}", _objects.Item1.Days_over_due__c.ToString());
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Days_over_due__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Debt_Management_Company__c))
            {
                findAndReplace(appWord, "{loan__c.Debt_Management_Company__c}", _objects.Item1.Debt_Management_Company__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Debt_Management_Company__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Debt_Management_Email__c))
            {
                findAndReplace(appWord, "{loan__c.Debt_Management_Email__c}", _objects.Item1.Debt_Management_Email__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Debt_Management_Email__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Debt_Management_Ref_No__c))
            {
                findAndReplace(appWord, "{loan__c.Debt_Management_Ref_No__c}", _objects.Item1.Debt_Management_Ref_No__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Debt_Management_Ref_No__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Debt_Management_Tel_No__c))
            {
                findAndReplace(appWord, "{loan__c.Debt_Management_Tel_No__c}", _objects.Item1.Debt_Management_Tel_No__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Debt_Management_Tel_No__c}", "");
            }

            if (_objects.Item1.Defaulted_date__c != null)
            {
                getDate("{loan__c.Defaulted_date__c}", _text, (DateTime)_objects.Item1.Defaulted_date__c, appWord);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Defaulted_date__c}", "");
            }

            if (_objects.Item1.Default_Notice_Date__c != null)
            {
                getDate("{loan__c.Default_Notice_Date__c}", _text, (DateTime)_objects.Item1.Default_Notice_Date__c, appWord);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Default_Notice_Date__c}", "");
            }

            if (_objects.Item1.Delay_Fee_Amount__c != null)
            {
                findAndReplace(appWord, "{loan__c.Delay_Fee_Amount__c}", ((double)_objects.Item1.Delay_Fee_Amount__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Delay_Fee_Amount__c}", "");
            }

            if (_objects.Item1.Delay_Interest_Amount__c != null)
            {
                findAndReplace(appWord, "{loan__c.Delay_Interest_Amount__c}", ((double)_objects.Item1.Delay_Interest_Amount__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Delay_Interest_Amount__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.DOB__c))
            {
                findAndReplace(appWord, "{loan__c.DOB__c}", _objects.Item1.DOB__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.DOB__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Email__c))
            {
                findAndReplace(appWord, "{loan__c.Email__c}", _objects.Item1.Email__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Email__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Email_Copy__c))
            {
                findAndReplace(appWord, "{loan__c.Email_Copy__c}", _objects.Item1.Email_Copy__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Email_Copy__c}", "");
            }

            if (_objects.Item1.Due_Date__c != null)
            {
                getDate("{loan__c.Due_Date__c}", _text, (DateTime)_objects.Item1.Due_Date__c, appWord);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Due_Date__c}", "");
            }

            if (_objects.Item1.Freeze_Start__c != null)
            {
                getDate("{loan__c.Freeze_Start__c}", _text, (DateTime)_objects.Item1.Freeze_Start__c, appWord);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Freeze_Start__c}", "");
            }

            if (_objects.Item1.Freeze_Stop__c != null)
            {
                getDate("{loan__c.Freeze_Stop__c}", _text, (DateTime)_objects.Item1.Freeze_Stop__c, appWord);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Freeze_Stop__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Full_Name__c))
            {
                findAndReplace(appWord, "{loan__c.Full_Name__c}", _objects.Item1.Full_Name__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Full_Name__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Healh_Check_Log__c))
            {
                findAndReplace(appWord, "{loan__c.Healh_Check_Log__c}", _objects.Item1.Healh_Check_Log__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Healh_Check_Log__c}", "");
            }

            if (_objects.Item1.Interest_After_Duedate__c != null)
            {
                findAndReplace(appWord, "{loan__c.Interest_After_Duedate__c}", ((double)_objects.Item1.Interest_After_Duedate__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Interest_After_Duedate__c}", "");
            }

            if (_objects.Item1.Interest_Amount__c != null)
            {
                findAndReplace(appWord, "{loan__c.Interest_Amount__c}", ((double)_objects.Item1.Interest_Amount__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Interest_Amount__c}", "");
            }

            if (_objects.Item1.Interest_Before_Duedate__c != null)
            {
                findAndReplace(appWord, "{loan__c.Interest_Before_Duedate__c}", ((double)_objects.Item1.Interest_Before_Duedate__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Interest_Before_Duedate__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.LAS_Loan_Id__c))
            {
                findAndReplace(appWord, "{loan__c.LAS_Loan_Id__c}", _objects.Item1.LAS_Loan_Id__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.LAS_Loan_Id__c}", "");
            }

            if (_objects.Item1.Last_Payment_Amount__c != null)
            {
                findAndReplace(appWord, "{loan__c.Last_Payment_Amount__c}", ((double)_objects.Item1.Last_Payment_Amount__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Last_Payment_Amount__c}", "");
            }

            if (_objects.Item1.Last_PaymentDate__c != null)
            {
                getDate("{loan__c.Last_PaymentDate__c}", _text, (DateTime)_objects.Item1.Last_PaymentDate__c, appWord);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Last_PaymentDate__c}", "");
            }

            if (_objects.Item1.Last_Payment_Date__c != null)
            {
                getDate("{loan__c.Last_Payment_Date__c}", _text, (DateTime)_objects.Item1.Last_Payment_Date__c, appWord);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Last_Payment_Date__c}", "");
            }

            if (_objects.Item1.Late_Fees__c != null)
            {
                findAndReplace(appWord, "{loan__c.Late_Fees__c}", ((double)_objects.Item1.Late_Fees__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Late_Fees__c}", "");
            }

            if (_objects.Item1.Loan_Amount__c != null)
            {
                findAndReplace(appWord, "{loan__c.Loan_Amount__c}", ((double)_objects.Item1.Loan_Amount__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Loan_Amount__c}", "");
            }

            if (_objects.Item1.Loan_Calculated_Date__c != null)
            {
                getDate("{loan__c.Loan_Calculated_Date__c}", _text, (DateTime)_objects.Item1.Loan_Calculated_Date__c, appWord);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Loan_Calculated_Date__c}", "");
            }

            if (_objects.Item1.Loan_counter__c != null)
            {
                findAndReplace(appWord, "{loan__c.Loan_counter__c}", _objects.Item1.Loan_counter__c.ToString());
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Loan_counter__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Marketing_Channel__c))
            {
                findAndReplace(appWord, "{loan__c.Marketing_Channel__c}", _objects.Item1.Marketing_Channel__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Marketing_Channel__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Mobile__c))
            {
                findAndReplace(appWord, "{loan__c.Mobile__c}", _objects.Item1.Mobile__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Mobile__c}", "");
            }

            if (_objects.Item1.Monthly_Net_Income__c != null)
            {
                findAndReplace(appWord, "{loan__c.Monthly_Net_Income__c}", ((double)_objects.Item1.Monthly_Net_Income__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Monthly_Net_Income__c}", "");
            }

            if (_objects.Item1.Monthly_Payment__c != null)
            {
                findAndReplace(appWord, "{loan__c.Monthly_Payment__c}", ((double)_objects.Item1.Monthly_Payment__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Monthly_Payment__c}", "");
            }

            if (_objects.Item1.Next_Call_Date__c != null)
            {
                getDate("{loan__c.Next_Call_Date__c}", _text, (DateTime)_objects.Item1.Next_Call_Date__c, appWord);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Next_Call_Date__c}", "");
            }

            if (_objects.Item1.Next_Contact_Date__c != null)
            {
                getDate("{loan__c.Next_Contact_Date__c}", _text, (DateTime)_objects.Item1.Next_Contact_Date__c, appWord);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Next_Contact_Date__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Number_Of_Payments__c))
            {
                findAndReplace(appWord, "{loan__c.Number_Of_Payments__c}", _objects.Item1.Number_Of_Payments__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Number_Of_Payments__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Old_SF_Customer_Id__c))
            {
                findAndReplace(appWord, "{loan__c.Old_SF_Customer_Id__c}", _objects.Item1.Old_SF_Customer_Id__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Old_SF_Customer_Id__c}", "");
            }

            if (_objects.Item1.Original_Due_Date__c != null)
            {
                getDate("{loan__c.Original_Due_Date__c}", _text, (DateTime)_objects.Item1.Original_Due_Date__c, appWord);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Original_Due_Date__c}", "");
            }

            if (_objects.Item1.Total_Outstanding_Amount__c != null)
            {
                findAndReplace(appWord, "{loan__c.Total_Outstanding_Amount__c}", ((double)_objects.Item1.Total_Outstanding_Amount__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Total_Outstanding_Amount__c}", "");
            }

            if (_objects.Item1.OutstandingBalanceMypage__c != null)
            {
                findAndReplace(appWord, "{loan__c.OutstandingBalanceMypage__c}", ((double)_objects.Item1.OutstandingBalanceMypage__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
            }
            else
            {
                findAndReplace(appWord, "{loan__c.OutstandingBalanceMypage__c}", "");
            }

            if (_objects.Item1.Loan_Days__c != null)
            {
                findAndReplace(appWord, "{loan__c.Loan_Days__c}", _objects.Item1.Loan_Days__c.ToString());
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Loan_Days__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Our_Status__c))
            {
                findAndReplace(appWord, "{loan__c.Our_Status__c}", _objects.Item1.Our_Status__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Our_Status__c}", "");
            }

            if (_objects.Item1.Outstanding_Balance__c != null)
            {
                findAndReplace(appWord, "{loan__c.Outstanding_Balance__c}", ((double)_objects.Item1.Outstanding_Balance__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Outstanding_Balance__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Outstanding_Balance_String__c))
            {
                findAndReplace(appWord, "{loan__c.Outstanding_Balance_String__c}", _objects.Item1.Outstanding_Balance_String__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Outstanding_Balance_String__c}", "");
            }

            if (_objects.Item1.Agent_Paid__c != null)
            {
                findAndReplace(appWord, "{loan__c.Agent_Paid__c}", ((double)_objects.Item1.Agent_Paid__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Agent_Paid__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Payment_Frequency__c))
            {
                findAndReplace(appWord, "{loan__c.Payment_Frequency__c}", _objects.Item1.Payment_Frequency__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Payment_Frequency__c}", "");
            }

            if (_objects.Item1.Payout_Date__c != null)
            {
                getDate("{loan__c.Payout_Date__c}", _text, (DateTime)_objects.Item1.Payout_Date__c, appWord);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Payout_Date__c}", "");
            }

            if (_objects.Item1.Principal__c != null)
            {
                findAndReplace(appWord, "{loan__c.Principal__c}", ((double)_objects.Item1.Principal__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Principal__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Returning_status__c))
            {
                findAndReplace(appWord, "{loan__c.Returning_status__c}", _objects.Item1.Returning_status__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Returning_status__c}", "");
            }

            if (_objects.Item1.Short_Settled_Sum__c != null)
            {
                findAndReplace(appWord, "{loan__c.Short_Settled_Sum__c}", ((double)_objects.Item1.Short_Settled_Sum__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Short_Settled_Sum__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Sort_Code__c))
            {
                findAndReplace(appWord, "{loan__c.Sort_Code__c}", _objects.Item1.Sort_Code__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Sort_Code__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Status__c))
            {
                findAndReplace(appWord, "{loan__c.Status__c}", _objects.Item1.Status__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Status__c}", "");
            }

            if (_objects.Item1.Swipe_Amount__c != null)
            {
                findAndReplace(appWord, "{loan__c.Swipe_Amount__c}", ((double)_objects.Item1.Swipe_Amount__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Swipe_Amount__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Third_Party_Company__c))
            {
                findAndReplace(appWord, "{loan__c.Third_Party_Company__c}", _objects.Item1.Third_Party_Company__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Third_Party_Company__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Third_Party_Email__c))
            {
                findAndReplace(appWord, "{loan__c.Third_Party_Email__c}", _objects.Item1.Third_Party_Email__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Third_Party_Email__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Third_Party_Ref_No__c))
            {
                findAndReplace(appWord, "{loan__c.Third_Party_Ref_No__c}", _objects.Item1.Third_Party_Ref_No__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Third_Party_Ref_No__c}", "");
            }

            if (_objects.Item1.Token_Amount__c != null)
            {
                findAndReplace(appWord, "{loan__c.Token_Amount__c}", ((double)_objects.Item1.Token_Amount__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Token_Amount__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.TS3_FileName__c))
            {
                findAndReplace(appWord, "{loan__c.TS3_FileName__c}", _objects.Item1.TS3_FileName__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.TS3_FileName__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.TS3_Processed_Time__c))
            {
                findAndReplace(appWord, "{loan__c.TS3_Processed_Time__c}", _objects.Item1.TS3_Processed_Time__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.TS3_Processed_Time__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.TS3_Reason__c))
            {
                findAndReplace(appWord, "{loan__c.TS3_Reason__c}", _objects.Item1.TS3_Reason__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.TS3_Reason__c}", "");
            }

            if (_objects.Item1.TS3_Received_Timestamp__c != null)
            {
                getDate("{loan__c.TS3_Received_Timestamp__c}", _text, (DateTime)_objects.Item1.TS3_Received_Timestamp__c, appWord);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.TS3_Received_Timestamp__c}", "");
            }

            if (_objects.Item1.TS3_Sent_Timestamp__c != null)
            {
                getDate("{loan__c.TS3_Sent_Timestamp__c}", _text, (DateTime)_objects.Item1.TS3_Sent_Timestamp__c, appWord);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.TS3_Sent_Timestamp__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.TS3_Status__c))
            {
                findAndReplace(appWord, "{loan__c.TS3_Status__c}", _objects.Item1.TS3_Status__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.TS3_Status__c}", "");
            }

            if (_objects.Item1.TS3_Status_Timestamp__c != null)
            {
                getDate("{loan__c.TS3_Status_Timestamp__c}", _text, (DateTime)_objects.Item1.TS3_Status_Timestamp__c, appWord);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.TS3_Status_Timestamp__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Workflow__c))
            {
                findAndReplace(appWord, "{loan__c.Workflow__c}", _objects.Item1.Workflow__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Workflow__c}", "");
            }

            if (_objects.Item1.Workflow_refresh__c != null)
            {
                findAndReplace(appWord, "{loan__c.Workflow_refresh__c}", _objects.Item1.Workflow_refresh__c.ToString());
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Workflow_refresh__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Workflow_Name__c))
            {
                findAndReplace(appWord, "{loan__c.Workflow_Name__c}", _objects.Item1.Workflow_Name__c);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Workflow_Name__c}", "");
            }

            if (_objects.Item1.Workflow_Next_Date__c != null)
            {
                getDate("{loan__c.Workflow_Next_Date__c}", _text, (DateTime)_objects.Item1.Workflow_Next_Date__c, appWord);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Workflow_Next_Date__c}", "");
            }

            if (_objects.Item1.Workflow_Reminder_Date__c != null)
            {
                getDate("{loan__c.Workflow_Reminder_Date__c}", _text, (DateTime)_objects.Item1.Workflow_Reminder_Date__c, appWord);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Workflow_Reminder_Date__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Name))
            {
                findAndReplace(appWord, "{loan__c.Name}", _objects.Item1.Name);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Name}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item1.Id))
            {
                findAndReplace(appWord, "{loan__c.Id}", _objects.Item1.Id);
            }
            else
            {
                findAndReplace(appWord, "{loan__c.Id}", "");
            }


            //Replace from applicant
            if (!String.IsNullOrEmpty(_objects.Item3.Address__c))
            {
                findAndReplace(appWord, "{Applicant__c.Address__c}", _objects.Item3.Address__c);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.Address__c}", "");
            }

            if (_objects.Item3.Date_Of_Birth__c != null)
            {
                getDate("{Applicant__c.Date_Of_Birth__c}", _text, (DateTime)_objects.Item3.Date_Of_Birth__c, appWord);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.Date_Of_Birth__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item3.Email__c))
            {
                findAndReplace(appWord, "{Applicant__c.Email__c}", _objects.Item3.Email__c);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.Email__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item3.First_Name__c))
            {
                findAndReplace(appWord, "{Applicant__c.First_Name__c}", _objects.Item3.First_Name__c);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.First_Name__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item3.Flat_Number__c))
            {
                findAndReplace(appWord, "{Applicant__c.Flat_Number__c}", _objects.Item3.Flat_Number__c);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.Flat_Number__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item3.Full_Name__c))
            {
                findAndReplace(appWord, "{Applicant__c.Full_Name__c}", _objects.Item3.Full_Name__c);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.Full_Name__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item3.Landline__c))
            {
                findAndReplace(appWord, "{Applicant__c.Landline__c}", _objects.Item3.Landline__c);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.Landline__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item3.LAS_Customer_Id__c))
            {
                findAndReplace(appWord, "{Applicant__c.LAS_Customer_Id__c}", _objects.Item3.LAS_Customer_Id__c);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.LAS_Customer_Id__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item3.Last_Name__c))
            {
                findAndReplace(appWord, "{Applicant__c.Last_Name__c}", _objects.Item3.Last_Name__c);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.Last_Name__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item3.LAS_User_Name__c))
            {
                findAndReplace(appWord, "{Applicant__c.LAS_User_Name__c}", _objects.Item3.LAS_User_Name__c);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.LAS_User_Name__c}", "");
            }

            if (_objects.Item3.Number_Of_Payment_Plan__c != null)
            {
                findAndReplace(appWord, "{Applicant__c.Number_Of_Payment_Plan__c}", _objects.Item3.Number_Of_Payment_Plan__c);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.Number_Of_Payment_Plan__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item3.Old_SF_Customer_Id__c))
            {
                findAndReplace(appWord, "{Applicant__c.Old_SF_Customer_Id__c}", _objects.Item3.Old_SF_Customer_Id__c);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.Old_SF_Customer_Id__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item3.Password__c))
            {
                findAndReplace(appWord, "{Applicant__c.Password__c}", _objects.Item3.Password__c);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.Password__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item3.Payment_Plan__c))
            {
                findAndReplace(appWord, "{Applicant__c.Payment_Plan__c}", _objects.Item3.Payment_Plan__c);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.Payment_Plan__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item3.Postal_Code__c))
            {
                findAndReplace(appWord, "{Applicant__c.Postal_Code__c}", _objects.Item3.Postal_Code__c);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.Postal_Code__c}", "");
            }

            if (_objects.Item3.Promise_Date__c != null)
            {
                getDate("{Applicant__c.Promise_Date__c}", _text, (DateTime)_objects.Item3.Promise_Date__c, appWord);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.Promise_Date__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item3.Promise_Option__c))
            {
                findAndReplace(appWord, "{Applicant__c.Promise_Option__c}", _objects.Item3.Promise_Option__c);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.Promise_Option__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item3.Residential_Status__c))
            {
                findAndReplace(appWord, "{Applicant__c.Residential_Status__c}", _objects.Item3.Residential_Status__c);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.Residential_Status__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item3.Street__c))
            {
                findAndReplace(appWord, "{Applicant__c.Street__c}", _objects.Item3.Street__c);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.Street__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item3.Title__c))
            {
                findAndReplace(appWord, "{Applicant__c.Title__c}", _objects.Item3.Title__c);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.Title__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item3.Town__c))
            {
                findAndReplace(appWord, "{Applicant__c.Town__c}", _objects.Item3.Town__c);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.Town__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item3.Work_Phone__c))
            {
                findAndReplace(appWord, "{Applicant__c.Work_Phone__c}", _objects.Item3.Work_Phone__c);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.Work_Phone__c}", "");
            }

            if (!String.IsNullOrEmpty(_objects.Item3.County__c))
            {
                findAndReplace(appWord, "{Applicant__c.County__c}", _objects.Item3.County__c);
            }
            else
            {
                findAndReplace(appWord, "{Applicant__c.County__c}", "");
            }


            //Repayment plan
            if (_objects.Item4 != null)
            {
                if (_objects.Item4.Arrangement_Date__c != null)
                {
                    getDate("{RePayment_Plan__c.Arrangement_Date__c}", _text, (DateTime)_objects.Item4.Arrangement_Date__c, appWord);
                }
                else
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Arrangement_Date__c}", "");
                }

                if (_objects.Item4.Defaulted_status_set__c != null)
                {
                    getDate("{RePayment_Plan__c.Defaulted_status_set__c}", _text, (DateTime)_objects.Item4.Defaulted_status_set__c, appWord);
                }
                else
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Defaulted_status_set__c}", "");
                }

                //if (!String.IsNullOrEmpty(_objects.Item4.Hidden_status__c)){findAndReplace(appWord, "{RePayment_Plan__c.Hidden_status__c}", _objects.Item4.Hidden_status__c);}
                //if (_objects.Item4.HIDDEN_RS_Date__c != null){findAndReplace(appWord, "{RePayment_Plan__c.HIDDEN_RS_Date__c}", _objects.Item4.HIDDEN_RS_Date__c);}
                //if (_objects.Item4.HIDDEN_RS_Payment__c != null){findAndReplace(appWord, "{RePayment_Plan__c.HIDDEN_RS_Payment__c}", _objects.Item4.HIDDEN_RS_Payment__c);}
                if (!String.IsNullOrEmpty(_objects.Item4.TimeSpan__c))
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.TimeSpan__c}", _objects.Item4.TimeSpan__c);
                }
                else
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.TimeSpan__c}", "");
                }

                if (!String.IsNullOrEmpty(_objects.Item4.Loan__c))
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Loan__c}", _objects.Item4.Loan__c);
                }
                else
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Loan__c}", "");
                }

                if (_objects.Item4.Outstanding_amount__c != null)
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Outstanding_amount__c}", ((double)_objects.Item4.Outstanding_amount__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
                }
                else
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Outstanding_amount__c}", "");
                }

                if (_objects.Item4.Payment_taken_on_Arrangement_Date__c != null)
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Payment_taken_on_Arrangement_Date__c}", _objects.Item4.Payment_taken_on_Arrangement_Date__c);
                }
                else
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Payment_taken_on_Arrangement_Date__c}", "");
                }

                if (!String.IsNullOrEmpty(_objects.Item4.Payment_type__c))
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Payment_type__c}", _objects.Item4.Payment_type__c);
                }
                else
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Payment_type__c}", "");
                }

                if (_objects.Item4.Repayment_Amount__c != null)
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Repayment_Amount__c}", ((double)_objects.Item4.Repayment_Amount__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
                }
                else
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Repayment_Amount__c}", "");
                }

                //if (_objects.Item4.Repayment_period__c != null){findAndReplace(appWord, "{RePayment_Plan__c.Repayment_period__c}", _objects.Item4.Repayment_period__c);}
                if (_objects.Item4.Short_settlement_amount__c != null)
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Short_settlement_amount__c}", ((double)_objects.Item4.Short_settlement_amount__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
                }
                else
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Short_settlement_amount__c}", "");
                }

                if (_objects.Item4.Start_date__c != null)
                {
                    getDate("{Applicant__c.Start_date__c}", _text, (DateTime)_objects.Item4.Start_date__c, appWord);
                }
                else
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Start_date__c}", "");
                }

                if (!String.IsNullOrEmpty(_objects.Item4.Status__c))
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Status__c}", _objects.Item4.Status__c);
                }
                else
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Status__c}", "");
                }

                if (_objects.Item4.Stop_interest_from_this_date__c != null)
                {
                    getDate("{Applicant__c.Stop_interest_from_this_date__c}", _text, (DateTime)_objects.Item4.Stop_interest_from_this_date__c, appWord);
                }
                else
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Stop_interest_from_this_date__c}", "");
                }

                if (!String.IsNullOrEmpty(_objects.Item4.Term__c))
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Term__c}", _objects.Item4.Term__c);
                }
                else
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Term__c}", "");
                }

                //if (!String.IsNullOrEmpty(_objects.Item4.Time_granulation__c)){findAndReplace(appWord, "{RePayment_Plan__c.Time_granulation__c}", _objects.Item4.Time_granulation__c);}
                //if (_objects.Item4.Timeframe__c != null){findAndReplace(appWord, "{RePayment_Plan__c.Timeframe__c}", _objects.Item4.Timeframe__c);}
                if (_objects.Item4.Total_Amount_Repayable__c != null)
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Total_Amount_Repayable__c}", ((double)_objects.Item4.Total_Amount_Repayable__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB")));
                }
                else
                {
                    findAndReplace(appWord, "{RePayment_Plan__c.Total_Amount_Repayable__c}", "");
                }
            }

            //break down
            if (_objects.Item5 != null)
            {
                if (_objects.Item5.PrincipalForInterestCalc__c != null)
                {
                    findAndReplace(appWord, "{Payment_Plan__c.PrincipalForInterestCalc__c}", _objects.Item5.PrincipalForInterestCalc__c);
                }
                else
                {
                    findAndReplace(appWord, "{Payment_Plan__c.PrincipalForInterestCalc__c}", "");
                }

                if (_objects.Item5.Outstanding_Amount__c != null && _objects.Item5.PrincipalForInterestCalc__c != null)
                {
                    findAndReplace(appWord, "{Payment_Plan__c.CompundCharges}", _objects.Item5.Outstanding_Amount__c - _objects.Item5.PrincipalForInterestCalc__c);
                }
                else
                {
                    findAndReplace(appWord, "{Payment_Plan__c.CompundCharges}", "");
                }
            }

            
        }

        private void findAndReplace(Microsoft.Office.Interop.Word.Application doc, object findText, object replaceWithTex)
        {
            object matchCase = false;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;

            doc.Selection.Find.Execute(ref findText, ref matchCase, ref matchWholeWord,
            ref matchWildCards, ref matchSoundsLike, ref matchAllWordForms, ref forward, ref wrap, ref format, ref replaceWithTex, ref replace,
            ref matchKashida, ref matchDiacritics, ref matchAlefHamza, ref matchControl);     
        }

        public void deleteFiles()
        {
            String filePath = setPath;
            
            var templateFiles = Directory.GetFiles(filePath + "templates\\");
            foreach (string item in templateFiles)
            {
                File.Delete(item);
            }
            
            var letterFiles = Directory.GetFiles(filePath + "letters\\");
            foreach (string item in letterFiles)
            {
                File.Delete(item);
            }
            
        }

        public void insertTable(Microsoft.Office.Interop.Word.Document _wordDocument, string _loanId)
        {
            SalesforceCommunicator sfComm = new SalesforceCommunicator();
            var fileList = sfComm.getPaymentTransactions(_loanId);

            if (fileList != null && fileList.Count > 0)
            {
                Word.Range r = _wordDocument.Range();
                r.SetRange(_wordDocument.Content.Text.IndexOf("{Table}"), _wordDocument.Content.Text.IndexOf("{Table}") + 7);
                Microsoft.Office.Interop.Word.Table objTab = _wordDocument.Tables.Add(r, fileList.Count + 1, 3, null, true);
                objTab.Cell(1, 1).Range.Text = "Transaction Id";
                objTab.Cell(1, 2).Range.Text = "Date";
                objTab.Cell(1, 3).Range.Text = "Amount";
                objTab.Rows[1].Range.Font.Bold = 1;
                objTab.Rows[1].Range.Font.Size = 11.0f;
                objTab.Rows.Borders.Enable = 1;
                objTab.Rows.Borders.OutsideLineWidth = WdLineWidth.wdLineWidth025pt;

                int counter = 2;
                foreach (EnterpriseWsdl.Payment__c item in fileList)
                {
                    objTab.Cell(counter, 1).Range.Text = item.Transaction_Id__c;
                    objTab.Cell(counter, 2).Range.Text = ((DateTime)item.Financial_Date__c).ToString("dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-GB"));
                    objTab.Cell(counter, 3).Range.Text = ((double)item.Amount__c).ToString("C2", CultureInfo.CreateSpecificCulture("en-GB"));
                    counter++;
                }
            }
        }

        private void getDate(string _searchText, string _text, DateTime _fieldDate, Microsoft.Office.Interop.Word.Application appWord)
        {
            //Find how many occurances there are
            List<int> occurances = new List<int>();
            int location = 0;
            bool searchedThrough = true;
            while(searchedThrough)
            {
                int tmp = _text.ToLower().IndexOf(_searchText.ToLower(), location);
                if (tmp > 0)
                {
                    occurances.Add(tmp);
                    if ((tmp + 1) < _text.Length )
                    {
                        location = tmp + 1;
                    }
                    else
                    {
                        searchedThrough = false;
                    }
                }
                else
                {
                    searchedThrough = false;
                }
            }

            //change the text
            foreach (int item in occurances)
            {
                if(_text[item-1] == '{')
                {
                    int endTag = _text.IndexOf('}', item + _searchText.Length);
                    string dateString = _text.Substring(item-1,endTag - (item - 2)).ToLower();
                    string tmpString = dateString.Replace(_searchText.ToLower(), " ");
                    tmpString = tmpString.Replace('{', ' ');
                    tmpString = tmpString.Replace('}', ' ');
                    tmpString = tmpString.Trim();
                    string[] values = tmpString.Split(' ');

                    string date;
                    if (values[0] == "+")
                    {
                        date = _fieldDate.AddDays(double.Parse(values[1])).ToString("dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-GB"));
                    }
                    else
                    {
                        date = _fieldDate.AddDays(-double.Parse(values[1])).ToString("dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-GB"));
                        
                    }
                    findAndReplace(appWord, dateString, date); 
                }
            }
            findAndReplace(appWord, _searchText, _fieldDate.ToString("dd MMMM, yyyy", CultureInfo.CreateSpecificCulture("en-GB"))); 
        }
    }
}
