﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Pdf_Generator
{
    class SalesforceCommunicator
    {

        private static string setPath
        {
            get
            {
                if (Directory.Exists(@"D:\Data\Cashflows Payments\"))
                {
                    return @"D:\Data\Cashflows Payments\";
                }
                else
                {
                    return @"C:\files\pdf\";
                }
            }
        }

        public List<Tuple<EnterpriseWsdl.Loan__c, EnterpriseWsdl.FileAttachment__c, EnterpriseWsdl.Applicant__c, EnterpriseWsdl.RePayment_Plan__c, EnterpriseWsdl.Payment_Plan__c>> getLoansForPdfGenerating()
        {
            List<SalesforceObjects> returnListx = new List<SalesforceObjects>();
            Log log = new Log();
            var loginObject = new Login();

            EnterpriseWsdl.sObject[] outreachArray = new EnterpriseWsdl.sObject[500];
            EnterpriseWsdl.SessionHeader header = new EnterpriseWsdl.SessionHeader();
            header.sessionId = loginObject.SessionId;
            EndpointAddress apiAddr = new EndpointAddress(loginObject.ServerUrl);
            List<Tuple<EnterpriseWsdl.Loan__c, EnterpriseWsdl.FileAttachment__c, EnterpriseWsdl.Applicant__c, EnterpriseWsdl.RePayment_Plan__c, EnterpriseWsdl.Payment_Plan__c>> resultList = new List<Tuple<EnterpriseWsdl.Loan__c, EnterpriseWsdl.FileAttachment__c, EnterpriseWsdl.Applicant__c, EnterpriseWsdl.RePayment_Plan__c, EnterpriseWsdl.Payment_Plan__c>>();
            
            
            String query = "select Id, Created__c, FileID__c, FileName__c, FileTemplate__c, FileType__c, Loan_name__c, Sent_date__c, Status__c from FileAttachment__c where Status__c = 'New'";
            
            try
            {
                using (EnterpriseWsdl.SoapClient apiClient = new EnterpriseWsdl.SoapClient("Soap", apiAddr))
                {
                    EnterpriseWsdl.QueryResult result;
                    apiClient.query(header, //sessionheader
                                        null, //queryoptions
                                        null, //mruheader
                                        null, //packageversionheader
                                        query, //SOQL query
                                        out result
                                        );

                    if (result.records != null)
                    {
                        foreach (EnterpriseWsdl.FileAttachment__c item in result.records)
                        {
                            SalesforceObjects returnObject = new SalesforceObjects(item.Loan_name__c);
                            //*******************GET THE LOAN OBJECT**********************************

                            EnterpriseWsdl.FileAttachment__c fileAttachmentObject = item;
                            string loanQueryLoan = @"select id, name, Agent__c, External_Collection_Company__c, External_Collection_Status__c, 
                                                    Amount_Paid__c, Applicant__c, ApplicationFormCheck__c, AppliedOn__c, Authority__c, Bank_Account__c, Delete__c,
                                                    Callcredit_Status__c, CallReportCheck__c, CallValidateBankAndIDCheck__c, CallValidateCardCheck__c, Close_Date__c,
                                                    Days_over_due__c, Debt_Management_Company__c, Debt_Management_Email__c, 
                                                    Debt_Management_Ref_No__c, Debt_Management_Tel_No__c, Defaulted_date__c, Default_Notice_Date__c, Delay_Fee_Amount__c, 
                                                    Delay_Interest_Amount__c, DOB__c, Email__c, Email_Copy__c, Due_Date__c, Freeze_Start__c, Freeze_Stop__c, 
                                                    Full_Name__c, Healh_Check_Log__c, Interest_After_Duedate__c, Interest_Amount__c, Interest_Before_Duedate__c, 
                                                    LAS_Loan_Id__c, Last_Payment_Amount__c, Last_PaymentDate__c, Last_Payment_Date__c, Late_Fees__c, Loan_Amount__c, Loan_Calculated_Date__c, 
                                                    Loan_counter__c, Marketing_Channel__c, Mobile__c, Monthly_Net_Income__c, Monthly_Payment__c, Next_Call_Date__c, Next_Contact_Date__c, Number_Of_Payments__c, 
                                                    Old_SF_Customer_Id__c, Original_Due_Date__c, Total_Outstanding_Amount__c, Loan_Days__c, Our_Status__c, OutstandingBalanceMypage__c, Outstanding_Balance__c, 
                                                    Outstanding_Balance_String__c, Agent_Paid__c, Payment_Frequency__c, Payout_Date__c, Principal__c, Returning_status__c, Short_Settled_Sum__c, Sort_Code__c, 
                                                    Status__c, Swipe_Amount__c, Third_Party_Company__c, Third_Party_Email__c, Third_Party_Ref_No__c, Third_Party_Tel_No__c, Token_Amount__c, TS3_FileName__c, 
                                                    TS3_Processed_Time__c, TS3_Reason__c, TS3_Received_Timestamp__c, TS3_Sent_Timestamp__c, TS3_Status__c, TS3_Status_Timestamp__c, Workflow__c, 
                                                    Workflow_refresh__c, Workflow_Name__c, Workflow_Next_Date__c, Workflow_Reminder_Date__c 
                                                    from loan__c where name = '" + item.Loan_name__c + "'";

                            EnterpriseWsdl.QueryResult resultLoan;
                            apiClient.query(header, //sessionheader
                                                null, //queryoptions
                                                null, //mruheader
                                                null, //packageversionheader
                                                loanQueryLoan, //SOQL query
                                                out resultLoan
                                                );

                            if (resultLoan.records != null)
                            {
                                returnObject.LoanObject = (EnterpriseWsdl.Loan__c)resultLoan.records[0];
                                //*********************************GET THE APLLICANT/CUSTOMER OBJECt*******************************************

                                EnterpriseWsdl.Loan__c loanObject = (EnterpriseWsdl.Loan__c)resultLoan.records[0];
                                string loanQueryApplicant = @"select id, name, Address__c, Bankruptcy__c, Blacklist__c,
                                                            Customer_In_Imprisoned__c, Date_Of_Birth__c, Email__c, First_Name__c, Flat_Number__c, Full_Name__c, Landline__c, LAS_Customer_Id__c, 
                                                            Last_Name__c, LAS_User_Name__c, County__c,
                                                            Number_Of_Payment_Plan__c, Old_SF_Customer_Id__c, Password__c, Payment_Plan__c, Postal_Code__c, Promise_Date__c, Promise_Option__c, 
                                                            Residential_Status__c, Street__c, Title__c, Town__c, Work_Phone__c 
                                                            from Applicant__c where id = '" + loanObject.Applicant__c + "'";
                                    
                                EnterpriseWsdl.QueryResult resultApplicant;
                                apiClient.query(header, //sessionheader
                                                    null, //queryoptions
                                                    null, //mruheader
                                                    null, //packageversionheader
                                                    loanQueryApplicant, //SOQL query
                                                    out resultApplicant
                                                    );
                                if (resultApplicant.records != null)
                                {
                                    returnObject.ApplicantObject = (EnterpriseWsdl.Applicant__c)resultApplicant.records[0];
                                    EnterpriseWsdl.Applicant__c applicantObject = (EnterpriseWsdl.Applicant__c)resultApplicant.records[0];

                                    //***********************************GET THE REPAYMENT PLAN OBJECT*********************************
                                    string loanQueryPaymentPLan = @"select id, name, Arrangement_Date__c, Defaulted_status_set__c, TimeSpan__c, Loan__c, Outstanding_amount__c, 
                                                                  Payment_taken_on_Arrangement_Date__c, Payment_type__c, Repayment_Amount__c,
                                                                  Short_settlement_amount__c, Start_date__c, Status__c, Stop_interest_from_this_date__c, Term__c, 
                                                                  Total_Amount_Repayable__c 
                                                                  from repayment_plan__c where Loan__c = '" + loanObject.Id + "'";
                                    EnterpriseWsdl.QueryResult resultPaymentPLan;
                                    apiClient.query(header, //sessionheader
                                                        null, //queryoptions
                                                        null, //mruheader
                                                        null, //packageversionheader
                                                        loanQueryPaymentPLan, //SOQL query
                                                        out resultPaymentPLan
                                                        );

                                    //***********************************GET THE PAYMENT OBJECTS******************************************

                                    string loanQueryPayment = @"select id, name, Amount__c ,Date__c, Days_over_due__c ,Fee_Paid__c ,Financial_Date__c ,Interest_Paid__c 
                                                                ,LAS_Loan_Id__c ,Loan__c ,Month_over_due__c ,Paid_By__c ,Principle_Paid__c ,Transaction_Id__c 
                                                                ,Type__c ,Week_over_due__c ,Workflow_Status__c
                                                                from Payment__c where Loan__c = '" + loanObject.Id + "'  order by date__c";
                                    EnterpriseWsdl.QueryResult resultPayment;
                                    apiClient.query(header, //sessionheader
                                                        null, //queryoptions
                                                        null, //mruheader
                                                        null, //packageversionheader
                                                        loanQueryPayment, //SOQL query
                                                        out resultPayment
                                                        );

                                    //**********************************GET THE BREAKDOWN OBJECT************************************************

                                    string loanQueryBreakdown = @"select id, name, Adjustment__c,Date__c,Days_After_Due_Date__c,Days_After_Payout_Date__c,DelayInterestDayCount__c,Fee__c	
                                                                ,Interest__c,Interest_After_Due_Date__c,InterestAndFeeForInterestCalc__c,Interest_Before_Due_Date__c,Loan__c	
                                                                ,Loan_Amount__c,Original_Paid_Amount_This_Day__c,Outstanding_Amount__c,Outstanding_Amount_Mypage__c,Owe_Amount__c
                                                                ,Paid_Amount_This_Day__c,Paid_Amount_Total__c,Principal__c,PrincipalForInterestCalc__c,shadowInterest__c
                                                                from Payment_Plan__c where Loan__c = '" + loanObject.Id + "'  order by date__c ";
                                    EnterpriseWsdl.QueryResult resultBreakdown;
                                    apiClient.query(header, //sessionheader
                                                        null, //queryoptions
                                                        null, //mruheader
                                                        null, //packageversionheader
                                                        loanQueryBreakdown, //SOQL query
                                                        out resultBreakdown
                                                        );


                                    EnterpriseWsdl.RePayment_Plan__c repaymentPlanObject = null;
                                    if (resultPaymentPLan.records != null)
                                    {
                                        returnObject.RepaymentObject = (EnterpriseWsdl.RePayment_Plan__c)resultPaymentPLan.records[0];
                                        repaymentPlanObject = (EnterpriseWsdl.RePayment_Plan__c)resultPaymentPLan.records[0];
                                    }

                                    EnterpriseWsdl.Payment_Plan__c paymentPlanObject = null;
                                    if (resultBreakdown.records != null)
                                    {
                                        foreach (var breakdownObject in resultBreakdown.records)
                                        {
                                            returnObject.BreakdownObjectList.Add((EnterpriseWsdl.Payment_Plan__c)breakdownObject); 
                                        }
                                        paymentPlanObject = (EnterpriseWsdl.Payment_Plan__c)resultBreakdown.records[0];
                                    }

                                    if (resultPayment.records != null)
                                    {
                                        foreach (var paymentObject in resultPayment.records)
                                        {
                                            returnObject.PaymentObjectsList.Add((EnterpriseWsdl.Payment__c)paymentObject);
                                        }
                                    }

                                    Tuple<EnterpriseWsdl.Loan__c, EnterpriseWsdl.FileAttachment__c, EnterpriseWsdl.Applicant__c, EnterpriseWsdl.RePayment_Plan__c, EnterpriseWsdl.Payment_Plan__c> returnTuple = 
                                        new Tuple<EnterpriseWsdl.Loan__c, EnterpriseWsdl.FileAttachment__c, EnterpriseWsdl.Applicant__c, EnterpriseWsdl.RePayment_Plan__c, EnterpriseWsdl.Payment_Plan__c>
                                            (loanObject, fileAttachmentObject, applicantObject, repaymentPlanObject, paymentPlanObject);

                                    returnListx.Add(returnObject);
                                    resultList.Add(returnTuple);
                                }
                            }
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                //log.LogError("Downloading loans from salesforce failed",
                //             "Pdf generator",
                //             ex.Message);
                log.LogSimpleEvent("Pdf Generator", "Error", "Downloading loans from salesforce failed", ex.Message);
            }
            return resultList;
        }

        public void uploadPdfs(Dictionary<string, byte[]> _letters)
        {
            Log log = new Log();
            var loginObject = new Login();

            EnterpriseWsdl.sObject[] outreachArrayCreate = new EnterpriseWsdl.sObject[_letters.Count];
            EnterpriseWsdl.sObject[] outreachArrayUpdate = new EnterpriseWsdl.sObject[_letters.Count];
            EnterpriseWsdl.SessionHeader header = new EnterpriseWsdl.SessionHeader();
            header.sessionId = loginObject.SessionId;
            EndpointAddress apiAddr = new EndpointAddress(loginObject.ServerUrl);
            EnterpriseWsdl.LimitInfo[] limitInfo;
            EnterpriseWsdl.SaveResult[] resultsCreate;
            EnterpriseWsdl.SaveResult[] resultsUpdate;

            try
            {
                using (EnterpriseWsdl.SoapClient apiClient = new EnterpriseWsdl.SoapClient("Soap", apiAddr))
                {
                    int counter = 0;
                    foreach (string item in _letters.Keys)
                    {
                        string query = "SELECT Name, id, FileName__c FROM FileAttachment__c where id = '" + item + "' ";

                        EnterpriseWsdl.QueryResult result;
                        apiClient.query(header, //sessionheader
                                            null, //queryoptions
                                            null, //mruheader
                                            null, //packageversionheader
                                            query, //SOQL query
                                            out result
                                            );

                        if (result.records != null)
                        {
                            EnterpriseWsdl.Attachment fileAttatchement = new EnterpriseWsdl.Attachment();
                            fileAttatchement.Body = _letters[item];
                            fileAttatchement.ParentId = ((EnterpriseWsdl.FileAttachment__c)(result.records[0])).Id;
                            fileAttatchement.Name = ((EnterpriseWsdl.FileAttachment__c)(result.records[0])).FileName__c + ".pdf";
                            outreachArrayCreate[counter] = fileAttatchement;

                            EnterpriseWsdl.FileAttachment__c FA = new EnterpriseWsdl.FileAttachment__c();
                            FA.Id = item;
                            //FA.Status__c = "Ready";
                            FA.Status__c = "Auto Approved";
                            outreachArrayUpdate[counter] = FA;
                            counter++;
                        }
                    }

                    Console.WriteLine("uploading files");

                    if (outreachArrayCreate[0] != null)
                    {
                        apiClient.create(
                                header, //sessionheader
                                null,  //assignmentruleheader
                                null,  //mruheader
                                null,  //allowfieldtruncationheader
                                null,  //disablefeedtrackingheader
                                null,  //streamingenabledheader
                                null,  //allornoneheader
                                null,
                                null,  //local options
                                null,  //debuggingheader
                                null,  //packageversionheader
                                null,  //emailheader
                                outreachArrayCreate, //objects to add
                                out limitInfo,
                                out resultsCreate //results of the creation operation
                                );

                        if (resultsCreate[0].success)
                        {
                            foreach (EnterpriseWsdl.Attachment item in outreachArrayCreate)
                            {
                                //log.logJob("Pdf file succsefully created", "Fileid: " + item.Id);
                                log.LogSimpleEvent("Pdf Generator", "Event", "Pdf file succsefully created", "Fileid: " + item.Id);
                            }
                        }
                        else
                        {
                            foreach (EnterpriseWsdl.SaveResult item in resultsCreate)
                            {
                            //    log.LogError("PDF files were not created successfully",
                            //                 "Attachement Uploader",
                            //                 "Message: " + item.errors[0].message + "\r\n" +
                            //                 "Fields: " + item.errors[0].fields + "\r\n" +
                            //                 "StatusCode: " + item.errors[0].statusCode);
                                log.LogSimpleEvent("Pdf Generator", "Error", "PDF files were not created successfully", "Message: " + item.errors[0].message + "\r\nFields: " + item.errors[0].fields + "\r\nStatusCode: " + item.errors[0].statusCode);
                            }
                        }
                    }


                    Console.WriteLine("Updating objects");

                    if (outreachArrayUpdate[0] != null)
                    {
                        apiClient.update(
                                header, //sessionheader
                                null,  //assignmentruleheader
                                null,  //mruheader
                                null,  //allowfieldtruncationheader
                                null,  //disablefeedtrackingheader
                                null,  //streamingenabledheader
                                null,  //allornoneheader
                                null,
                                null,  //local options
                                null,  //debuggingheader
                                null,  //packageversionheader
                                null,  //emailheader
                                null,
                                outreachArrayUpdate, //objects to add
                                out limitInfo,
                                out resultsUpdate //results of the creation operation
                                );

                        if (resultsUpdate[0].success)
                        {
                            foreach (EnterpriseWsdl.FileAttachment__c item in outreachArrayUpdate)
                            {
                                //log.logJob("FileAttachment object succsefully updated", "Filename: " + item.Name);
                                log.LogSimpleEvent("Pdf Generator", "Event", "FileAttachment object succsefully updated", "Filename: " + item.Name);
                            }
                        }
                        else
                        {
                            foreach (EnterpriseWsdl.SaveResult item in resultsUpdate)
                            {
                                //log.LogError("FileAttachment object were not updated successfully",
                                //             "Pdf generator",
                                //             "Message: " + item.errors[0].message + "\r\n" +
                                //             "Fields: " + item.errors[0].fields + "\r\n" +
                                //             "StatusCode: " + item.errors[0].statusCode);
                                log.LogSimpleEvent("Pdf Generator", "Error", "FileAttachment object were not updated successfully", "Message: " + item.errors[0].message + "\r\nFields: " + item.errors[0].fields + "\r\nStatusCode: " + item.errors[0].statusCode);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //log.LogError("FileAttachment object were not updated successfully",
                //             "Pdf generator",
                //             ex.Message);
                log.LogSimpleEvent("Pdf Generator", "Error", "FileAttachment object were not updated successfully", ex.Message);
            }
        }

        public void downloadTemplates()
        {
            Log log = new Log();
            Console.WriteLine("Login object");
            var loginObject = new Login();
            String filePath = setPath;
            Console.WriteLine("Path: " + filePath);
            Console.WriteLine("Set endpoints");
            EnterpriseWsdl.sObject[] outreachArray = new EnterpriseWsdl.sObject[500];
            EnterpriseWsdl.SessionHeader header = new EnterpriseWsdl.SessionHeader();
            header.sessionId = loginObject.SessionId;
            EndpointAddress apiAddr = new EndpointAddress(loginObject.ServerUrl);

            String query = "select Id, name, name__c from Letter_Template__c";

            try
            {
                using (EnterpriseWsdl.SoapClient apiClient = new EnterpriseWsdl.SoapClient("Soap", apiAddr))
                {
                    EnterpriseWsdl.QueryResult result;
                    apiClient.query(header, //sessionheader
                                        null, //queryoptions
                                        null, //mruheader
                                        null, //packageversionheader
                                        query, //SOQL query
                                        out result
                                        );

                    if (result.records != null)
                    {
                        foreach (EnterpriseWsdl.Letter_Template__c item in result.records)
                        {
                            String queryAttachment = "select Id, name, body from Attachment where ParentId = '" + item.Id + "'";

                            using (EnterpriseWsdl.SoapClient apiClientAttachement = new EnterpriseWsdl.SoapClient("Soap", apiAddr))
                            {
                                EnterpriseWsdl.QueryResult resultAttachement;
                                apiClientAttachement.query(header, //sessionheader
                                                    null, //queryoptions
                                                    null, //mruheader
                                                    null, //packageversionheader
                                                    queryAttachment, //SOQL query
                                                    out resultAttachement
                                                    );

                                if (resultAttachement.records != null)
                                {
                                    foreach (EnterpriseWsdl.Attachment itemAttachment in resultAttachement.records)
                                    {
                                        File.WriteAllBytes(setPath + "templates\\" + item.Name__c, itemAttachment.Body);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                //log.LogError("Template was not downloaded correctly",
                //                "Pdf generator",
                //                ex.Message);
                log.LogSimpleEvent("Pdf Generator", "Error", "Template was not downloaded correctly", ex.Message);
            }
        }

        public List<EnterpriseWsdl.Payment__c> getPaymentTransactions(string _loanId)
        {
            Log_Class log = new Log_Class();
            Console.WriteLine("Login object");
            var loginObject = new Login();
            List<EnterpriseWsdl.Payment__c> returnList = new List<EnterpriseWsdl.Payment__c>();

            Console.WriteLine("Set endpoints");
            EnterpriseWsdl.SessionHeader header = new EnterpriseWsdl.SessionHeader();
            header.sessionId = loginObject.SessionId;
            EndpointAddress apiAddr = new EndpointAddress(loginObject.ServerUrl);

            DateTime searchDate = DateTime.Today.AddYears(-1);

            String query = @"select Id, 
                                    name, 
                                    Amount__c, 
                                    Date__c, 
                                    Financial_Date__c, 
                                    LAS_Loan_Id__c, 
                                    Loan__c, 
                                    Paid_By__c, 
                                    Transaction_Id__c, 
                                    Type__c, 
                                    Workflow_Status__c 
                                    from Payment__c where 
                                    loan__c = '" + _loanId + "' ORDER BY Financial_Date__c ASC";
            try
            {
                using (EnterpriseWsdl.SoapClient apiClient = new EnterpriseWsdl.SoapClient("Soap", apiAddr))
                {
                    EnterpriseWsdl.QueryResult result;
                    apiClient.query(header, //sessionheader
                                        null, //queryoptions
                                        null, //mruheader
                                        null, //packageversionheader
                                        query, //SOQL query
                                        out result
                                        );

                    if (result.records != null)
                    {
                        foreach (EnterpriseWsdl.Payment__c item in result.records)
                        {
                            if (item.Financial_Date__c >= searchDate)
                            {
                                returnList.Add(item);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.LogError("Template was not downloaded correctly",
                                "Pdf generator",
                                ex.Message);
            }
            return returnList;
        }

        public List<EnterpriseWsdl.Payment_Plan__c> getPaymentPlanObject(string _loanId)
        {
            Log_Class log = new Log_Class();
            var loginObject = new Login();
            List<EnterpriseWsdl.Payment_Plan__c> returnList = new List<EnterpriseWsdl.Payment_Plan__c>();

            EnterpriseWsdl.SessionHeader header = new EnterpriseWsdl.SessionHeader();
            header.sessionId = loginObject.SessionId;
            EndpointAddress apiAddr = new EndpointAddress(loginObject.ServerUrl);

            DateTime searchDate = DateTime.Today.AddYears(-1);

            String query = @"select Id 
                            ,Adjustment__c
                            ,Date__c
                            ,Days_After_Due_Date__c
                            ,Days_After_Payout_Date__c
                            ,DelayInterestDayCount__c
                            ,Interest__c
                            ,Interest_After_Due_Date__c
                            ,InterestAndFeeForInterestCalc__c
                            ,Interest_Before_Due_Date__c
                            ,Loan__c
                            ,Loan_Amount__c
                            ,Original_Paid_Amount_This_Day__c
                            ,Outstanding_Amount__c
                            ,Outstanding_Amount_Mypage__c
                            ,Owe_Amount__c
                            ,Paid_Amount_This_Day__c
                            ,Paid_Amount_Total__c
                            ,Principal__c
                            ,PrincipalForInterestCalc__c
                            ,shadowInterest__c
                            from Payment__c where 
                            Payment_Plan__c = '" + _loanId + "' ORDER BY Date__c ASC";
            try
            {
                using (EnterpriseWsdl.SoapClient apiClient = new EnterpriseWsdl.SoapClient("Soap", apiAddr))
                {
                    EnterpriseWsdl.QueryResult result;
                    apiClient.query(header, //sessionheader
                                        null, //queryoptions
                                        null, //mruheader
                                        null, //packageversionheader
                                        query, //SOQL query
                                        out result
                                        );

                    if (result.records != null)
                    {
                        foreach (EnterpriseWsdl.Payment_Plan__c item in result.records)
                        {
                            if (item.Date__c >= searchDate)
                            {
                                returnList.Add(item);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.LogError("Template was not downloaded correctly",
                                "Pdf generator",
                                ex.Message);
            }
            return returnList;
        }

        public void getChargedIntrests(string _loanId)
        {
            List<Tuple<DateTime, double>> intrestList = new List<Tuple<DateTime, double>>();
            List<Tuple<DateTime, double>> feeList = new List<Tuple<DateTime, double>>();
            List<EnterpriseWsdl.Payment_Plan__c> objectList = getPaymentPlanObject(_loanId);

            foreach (EnterpriseWsdl.Payment_Plan__c item in objectList)
            {
                intrestList.Add(new Tuple<DateTime, double>((DateTime)item.Date__c, (double)item.Outstanding_Amount__c - (double)item.PrincipalForInterestCalc__c));

            }
        }
    }
}
