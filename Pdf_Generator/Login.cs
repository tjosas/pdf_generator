﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdf_Generator
{
    class Login
    {
        public string SessionId { get; set; }
        public string ServerUrl { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public Login()
        {
            Log_Class log = new Log_Class();
            try
            {
                //Get the username and password form the zip
                string pass = "";
                string user = "";
                ZipPassword.ZipPassword.getPassword("SalesForceTest", out user, out pass);
                //ZipPassword.ZipPassword.getPassword("SalesForce", out user, out pass);
                UserName = user;
                Password = pass;
                LoginToSalesForce();
            }
            catch (Exception e)
            {
                log.LogError("An error occured when trying to log into Salesforce", "Attachement Uploader", "Message: " + e.Message);
            }
        }

        private void LoginToSalesForce()
        {
            using (EnterpriseWsdl.SoapClient loginClient = new EnterpriseWsdl.SoapClient())
            {
                EnterpriseWsdl.LoginResult result = loginClient.login(null, UserName, Password);

                SessionId = result.sessionId;
                ServerUrl = result.serverUrl;
            }
        }
    }
}
